'use strict';
angular.module('Nqliast')
  .controller('IndexController', ['$scope', '$location', '$state', 'toaster', '$upload', '$stateParams', '$filter',
    function ($scope, $location, $state, toaster, $upload, $stateParams, $filter) {


        $scope.init = function () {
           
            if (sessionStorage.getItem("Data") != undefined) {
                $scope.DB = (JSON.parse(sessionStorage.getItem("Data")));
            } else {
                $scope.DB = [{
                                "UserId" : 1,
                                "FirstName": "Jaskaran",
                                "LastName": "Bir Singh",
                                "ContactNo": "8054171297",
                                "Email": "jbsdhami@gmal.com",
                                "Address": "295 sector 86"
                            }]


                sessionStorage.setItem("Data", JSON.stringify($scope.DB))
            }
        }

        $scope.DeleteData = function (emp) {
            var index = $scope.DB.indexOf(emp);
            $scope.DB.splice(index, 1);
            toaster.pop('success', "User deleted successfully");
            sessionStorage.setItem("Data", JSON.stringify($scope.DB))
        }
       

    }]);
