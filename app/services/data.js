'use strict';
angular.module('Nqliast')
    .factory('Services', ['$http', function ($http) {
        var baseUrl = NqliatServices;
        return {
            login: function (data, success, error) {
                $http.post(baseUrl + '/customer/login', data).success(success).error(error)
            },
            customerSignUp: function (data, success, error) {
                $http.post(baseUrl + '/customer/customerSignUp', data).success(success).error(error)
            },
             addQuotation: function (header,data, success, error) {
                $http.post(baseUrl + '/customer/addQuotation',data,{headers: header}).success(success).error(error)
            },
            openQuotationLisitng: function (header,data, success, error) {
                $http.post(baseUrl + '/customer/openQuotationLisitng',data,{headers: header}).success(success).error(error)
            },
             closeBidQuotation: function (data, success, error) {
                $http.post(baseUrl + '/customer/closeBidQuotation', data).success(success).error(error)
            },            
            Transporterlogin: function (data, success, error) {
                $http.post(baseUrl + '/transporter/login', data).success(success).error(error)
            },
            transporterSignUp: function (data, success, error) {
                $http.post(baseUrl + '/transporter/transporterSignUp', data).success(success).error(error)
            },
           
            getlanguage: function (data, success, error) {
                $http.post(baseUrl + '/admin/getlanguage', data).success(success).error(error)
            },

        }
    }])

