'use strict';
var myApp = angular.module('Nqliast', [
'ui.router', 'ngAnimate', 'toaster', 'ngFileUpload', 'ngTable', 'ui.bootstrap', 'infinite-scroll',
'angularjs-dropdown-multiselect','ngCookies','rmDatepicker','socialLogin'
])

myApp.config(function(socialProvider){
    socialProvider.setLinkedInKey("811mq8rxj2852e");
});

//local
// var NqliatServices = "http://192.168.100.23:8002/api";

//live
var NqliatServices = "http://34.210.240.189:8002/api";
