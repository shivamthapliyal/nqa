myApp.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
	$urlRouterProvider.otherwise('nq/home');

	$stateProvider
		.state('nq', {
			abstract: true,
			url: "/nq",
			templateUrl: "app/views/common/content.html"
		})
		.state('nq.home', {
			url: '/home',
			templateUrl: 'app/views/home.html',
			controller: 'HomeController'
		})
		.state('app', {
			abstract: true,
			url: "/app",
			templateUrl: "app/views/common/content2.html"
		})

		.state('/header', {
			url: '/header',
			templateUrl: 'app/views/common/header.html',
			//  controller: 'HomeController'
		})
		.state('/footer', {
			url: '/footer',
			templateUrl: 'app/views/common/footer.html',
			//controller: 'IndexController'
		})
		.state('app.request-quotation', {
			url: '/request-quotation',
			templateUrl: 'app/views/RequestQuotcustomersetting.html',
			controller: 'RequestQuotationCustomerController'
		})
		.state('app.my-quotations', {
			url: '/my-quotations',
			templateUrl: 'app/views/my-quotations.html',
			controller: 'MyQuotationCustomerController'
		})
		.state('app.messages', {
			url: '/messages',
			templateUrl: 'app/views/message.html',
			controller: 'MessagesCustomerController'
		})
		.state('app.profile-settings', {
			url: '/profile-settings',
			templateUrl: 'app/views/profile-settings.html',
			controller: 'ProfileCustomerController'
		})
		.state('app.trasporter-profile-settings', {
			url: '/trasporter-profile-settings',
			templateUrl: 'app/views/trasporter-profile-settings.html',
			controller: 'ProfileTransporterController'
		})
		.state('app.my-bids', {
			url: '/my-bids',
			templateUrl: 'app/views/my-bids.html',
		 	controller: 'MyBidsTranporterController'
		})
		.state('app.my-messages', {
			url: '/my-messages',
			templateUrl: 'app/views/transporter-messages.html',
			//	controller: 'ProfileCustomerController'
		})
		.state('app.customer-request', {
			url: '/customer-request',
			templateUrl: 'app/views/find-customer-request.html',
			//	controller: 'ProfileCustomerController'
		})


})
	.run(['$rootScope', '$window', '$location', function ($rootScope, $window, $location) {

		$rootScope.$on("$locationChangeStart", function (event, nextUrl, currentUrl) {

			$window.ga && $window.ga('send', 'pageview', { 'page': nextUrl });
		});
	}]);
