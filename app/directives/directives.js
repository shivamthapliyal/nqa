
myApp.directive('pageTitle', function ($rootScope, $timeout) {
    return {
        link: function (scope, element) {
            var listener = function (event, toState, toParams, fromState, fromParams) {
                var title = 'BHive | ';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'BHive | ' + toState.data.pageTitle;
                $timeout(function () {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
}
);

myApp.directive('allowPattern', function () {
    return {
        restrict: "A",
        compile: function (tElement, tAttrs) {
            return function (scope, element, attrs) {

                element.bind("keypress", function (event) {
                    var keyCode = event.which || event.keyCode;
                    var keyCodeChar = String.fromCharCode(keyCode);
                    if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                        event.preventDefault();
                        return false;
                    }

                });
            };
        }
    };
});

myApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return;
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
myApp.directive('numberMask', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).numeric();
        }
    }
});
myApp.filter('underscoreless', function () {
    return function (input) {
        return input.replace(/_/g, ' ');
    };
});

myApp.directive('validFile', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attrs, ngModel) {
            //change event is fired when file is selected
            el.bind('change', function () {
                scope.$myApply(function () {
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});

myApp.directive('ngSpace', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 32) {
                scope.$myApply(function () {
                    scope.$eval(attrs.ngSpace);
                });
                event.preventDefault();
            }
        });
    };
});

myApp.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function (val) {
                return val != null ? '' + val : null;
            });
        }
    };
});

myApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

myApp.directive('uixBxslider', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr, controller) {
            scope.$on('slider', function () {
                element.bxSlider(scope.$eval('{' + attr.uixBxslider + '}'));
            });
        }
    }
});

myApp.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

myApp.directive('capitalizeFirst', function (uppercaseFilter, $parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var capitalize = function (inputValue) {
                var capitalized = inputValue.charAt(0).toUpperCase() +
                    inputValue.substring(1);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            };
            var model = $parse(attrs.ngModel);
            modelCtrl.$parsers.push(capitalize);
            capitalize(model(scope));
        }
    };
});

myApp.directive('toggleSidebar', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $timeout(function () {
                element.bind("click", function (event) {
                    var toggleWidth = angular.element("#mySidenav").width() == 250 ? "0px" : "250px";
                    angular.element('#mySidenav').animate({ width: toggleWidth });
                });
            });
        }
    }
});

myApp.filter('truncate', function () {
    return function (text, length, end) {
        if (!text) {
            return;
        }
        if (isNaN(length)) {
            length = 10;
        }

        end = end || "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        } else {
            return String(text).substring(0, length - end.length) + end;
        }

    };
});

myApp.directive('scroll', function ($window) {
    return function (scope, element, attrs) {
        angular.element($window).bind("scroll", function () { 
            if (this.pageYOffset >= 500) {               
                scope.boolChangeClass = true;
                 $(".navbar").addClass('navback');
                   $(".navbar li .scrollA").css("color", "#000");
                $(".navbar-header a").html('<img src="img/assets/nqliat_logo_black.png">');
            } else {
                scope.boolChangeClass = false;
                 $(".navbar-header a").html('<img src="img/assets/nqliat_logo.png">');
                 $(".navbar li .scrollA").css("color", "#fff");
            }
            scope.$apply();
        });
    };
});