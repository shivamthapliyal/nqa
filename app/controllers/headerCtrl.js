'use strict';
angular.module('Nqliast')
    .controller('headerCtrl', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $cookies) {

            $scope.init = function () {
                // $scope.Accesstoken = sessionStorage.getItem("Accesstoken");
                // console.log("$scope.Accesstoken" + $scope.Accesstoken);

                if (typeof ('storage') != undefined) {

                    if (sessionStorage.getItem("LoggedInUser") != undefined) {
                        $scope.LoggedUserData = sessionStorage.getItem("LoggedInUser");

                        // console.log($scope.LoggedUserData);
                    }
                }

                $scope.Accesstoken = $cookies.get('token');
                // console.log("$scope.token" + $scope.Accesstoken);

                $scope.LoginVia = $cookies.get('CustorTrans');
                console.log("$scope.LoginVia :" + $scope.LoginVia);


                // window.scrollTo(0, 0);

            }
            $scope.logout = function () {
                $cookies.remove("token")
                sessionStorage.removeItem("Accesstoken");
                $state.go('np.home', {})
                $state.go('np.home');
                console.log("logounttt")
                // location.reload();
                //$state.go('header', {}, { reload: true });

            }


            $scope.LoginNavigation = function (details) {
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'LoginCust.html',
                    controller: 'LoginCustCtrl',
                    size: '',
                    resolve: {
                        items: function () {
                            return {
                                details: details
                            };
                        }
                    }
                });

            };

            $scope.SignupNavigation = function (details) {
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'SignUp.html',
                    controller: 'LoginCustCtrl',
                    size: '',
                    resolve: {
                        items: function () {
                            return {
                                details: details
                            };
                        }
                    }
                });

            };

            $scope.TransporterNavigation = function (details) {
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'TransporterFlow.html',
                    controller: 'TransporterFlowCtrl',
                    size: '',
                    resolve: {
                        items: function () {
                            return {
                                details: details
                            };
                        }
                    }
                });

            };
            $scope.logout = function (details) {
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'Logout.html',
                    controller: 'LogoutCtrl',
                    size: '',
                    resolve: {
                        items: function () {
                            return {
                                details: details
                            };
                        }
                    }
                });

            };



        }])



    .controller('LoginCustCtrl', ['$scope', '$rootScope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$uibModalInstance', '$cookies',
        function ($scope, $rootScope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $uibModalInstance, $cookies) {

            $scope.init = function () {

                GetLanguages();
            }
            $scope.CompanyCountry = "India";
            $scope.languageoptions = [];
            $scope.languageselected = [];

            $scope.settings = {
                scrollable: true,
                scrollableHeight: "250px",
                enableSearch: true,
                externalIdProp: ''
            }


            // $rootScope.$on('event:social-sign-in-success2', function (event, userDetails) {
            //     console.log(userDetails);

            //     // $scope.customer.email = userDetails.email;
            //     //  $scope.customer.password = userDetails.uRr7rvej8H;;
            //     console.log(userDetails.email)
            //     console.log(userDetails.uid)
            //     $scope.LoginCustomer();
            // })

            //Jaskaran - function to get all the languages
            function GetLanguages() {
                var data = {
                    skip: 0,
                    limit: 0
                };

                console.log(data);
                //return false;
                Services.getlanguage(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {

                        for (var i = 0; i < result.data.length; i++) {
                            var obj = result.data[i];
                            var objl = {};
                            objl.id = obj.name;
                            objl.label = obj.name

                            $scope.languageoptions.push(objl);
                        }
                        //console.log($scope.languageoptions)
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }

                });
            }

            //Jaskaran - function to close the modal
            $scope.close = function () {
                $uibModalInstance.dismiss('close');
            };

            var LinkData = {
                'email': '',
                'linkedInId': ''
            };

            $scope.customer = {
                Name: '',
                Signemail: ''
            };
            $rootScope.$on('event:social-sign-in-success2', function (event, userDetails) {
                console.log(userDetails);
                LinkData.email = userDetails.email;
                LinkData.linkedInId = userDetails.uid;
                $cookies.put('Linkedinidd', userDetails.uid)
                console.log(LinkData);

                Services.login(LinkData, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        toaster.pop('success', "Login successful");
                        sessionStorage.setItem("Accesstoken", result.data.accessToken);
                        $cookies.put('token', result.data.accessToken);
                        $cookies.put('CustorTrans', "Customer");
                        sessionStorage.setItem("LoggedInUser", JSON.stringify(result.data, undefined, 2));
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.pop('info', "You need to Sign up");
                        $("#home").hide();
                        $("#signupcustomerform1").show();
                        $scope.customer.Signemail = LinkData.email;
                        $scope.customer.Name = userDetails.name;

                    }
                });

            })




            $scope.LoginCustomer = function () {
                var data = {
                    email: $scope.customer.email,
                    password: $scope.customer.password
                };

                console.log(data);
                //return false;
                Services.login(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        toaster.pop('success', "Login successful");
                        sessionStorage.setItem("Accesstoken", result.data.accessToken);
                        $cookies.put('token', result.data.accessToken);
                        $cookies.put('CustorTrans', "Customer");
                        sessionStorage.setItem("LoggedInUser", JSON.stringify(result.data, undefined, 2));
                        location.reload();
                        // toaster.pop('success', result.message);
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', "Invalid email id or password");
                        //toaster.pop('error', error.message);
                    }
                });
            }
            //Jaskaran - function to sign up customer
            $scope.SignUpCustomer = function () {

                var objOflanguages = [];
                for (var k = 0; k < $scope.languageselected.length; k++) {
                    objOflanguages.push($scope.languageselected[k].id);
                    //  var ClassSectionString = objOflanguages.toString();
                    //  var ClassSectionString = objOflanguages;
                }
                var subscribe;
                if ($scope.customer.Subscribe == true) {
                    subscribe = "true";
                } else {
                    subscribe = "false";
                }

     var vialink  = "";    
     console.log($cookies.get('Linkedinidd'))      
if($cookies.get('Linkedinidd')!= ''){
vialink = $cookies.get('Linkedinidd');
}else{
vialink = false;
}

                var data = {
                    name: $scope.customer.Name,
                    email: $scope.customer.Signemail,
                    contact: $scope.customer.phone,
                    headquarters: $scope.customer.Headquarters,
                    company: $scope.customer.CompanyName,
                    password: $scope.customer.Signpassword,
                    companyEmail: $scope.customer.CompanyEmail,
                    country: $scope.CompanyCountry,
                    subscribed: subscribe,
                    viaLinkedIn: vialink,
                    languages: objOflanguages,
                    //linkedInId: ""
                };

                console.log(JSON.stringify(data, undefined, 2));
               //  return false;
                Services.customerSignUp(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        if (result.statusCode == 200) {
                            toaster.pop('success', "Signup successful");
                            $cookies.put('token', result.data.accessToken);
                            $cookies.put('CustorTrans', "Customer");
                            location.reload();
                        }
                        //toaster.pop('success', result.message);
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }
                });
            }


        }])


    .controller('TransporterFlowCtrl', ['$rootScope', '$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$uibModalInstance', '$cookies',
        function ($rootScope, $scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $uibModalInstance, $cookies) {

            $scope.init = function () {
                GetLanguages();
            }
            ///-----------------Equipments array
            $scope.TransportEquipments = [{
                id: 1,
                name: 'Car Transporter',
                pic: 'img/assets/car_transporter_home_page.png',
                class: 'pull-right'
            },
            {
                id: 2,
                name: 'Bus',
                pic: 'img/assets/bus_homepage.png',
                class: 'pull-left'
            },
            {
                id: 3,
                name: 'Tipper',
                pic: 'img/assets/tipper_homepage.png',
                class: 'pull-right'
            },
            {
                id: 4,
                name: 'Reefer',
                pic: 'img/assets/reefer_homepage.png',
                class: 'pull-left'
            },
            {
                id: 5,
                name: 'Tanker',
                pic: 'img/assets/tanker_homepage.png',
                class: 'pull-right'
            },
            {
                id: 6,
                name: 'Lowbed',
                pic: 'img/assets/lowbed_homepage.png',
                class: 'pull-left'
            },
            {
                id: 7,
                name: 'Flatbed',
                pic: 'img/assets/flatbed_homepage.png',
                class: 'pull-right'
            },
            {
                id: 8,
                name: 'Curtainside',
                pic: 'img/assets/curtainside_homepage.png',
                class: 'pull-left'
            },
            {
                id: 9,
                name: 'Container',
                pic: 'img/assets/container_homepage.png',
                class: 'pull-right'
            },
            {
                id: 10,
                name: 'Pickup',
                pic: 'img/assets/pickup_homepage.png',
                class: 'pull-left'
            }];

            // console.log(JSON.stringify($scope.TransportEquipments, undefined, 2));

            // ------------------------Category Product array -----------------------
            $scope.CategoryProduct = [{
                id: 1,
                name: 'Palletized Cargo',
                pic: 'img/assets/pallitized_goodsf.png'
            },
            {
                id: 2,
                name: 'Food Items',
                pic: 'img/assets/food_items.png'
            },
            {
                id: 3,
                name: 'Dangerous Goods',
                pic: 'img/assets/dangerous_goods.png'
            },
            {
                id: 4,
                name: 'Construction Materials',
                pic: 'img/assets/construction_materials.png'
            },
            {
                id: 5,
                name: 'Machinery',
                pic: 'img/assets/machinery.png'
            },
            {
                id: 6,
                name: 'Others',
                pic: 'img/assets/others.png'
            },
            {
                id: 7,
                name: 'Steel Bars & Pipes',
                pic: 'img/assets/steel_bars.png'
            },
            {
                id: 8,
                name: 'Bulk materials',
                pic: 'img/assets/bulk_materials.png'
            }];
            // jaskaran - international cities array
            $scope.InternationCities = [{
                id: 1,
                name: 'Rome'
            },
            {
                id: 2,
                name: 'Dubai'
            },
            {
                id: 3,
                name: 'Cairo'
            },
            {
                id: 4,
                name: 'Belgium'
            }];
            // console.log(JSON.stringify($scope.InternationCities, undefined, 2));

            // Jaskaran - function to save selected Equipments while signing up transporter
            $scope.SaveEquipments = function () {


                $scope.equipmentsArray = [];
                angular.forEach($scope.TransportEquipments, function (Equipments) {
                    if (Equipments.selected) $scope.equipmentsArray.push(Equipments.name);
                });

                if ($scope.equipmentsArray == "") {
                    toaster.pop('error', "You need to select atleast one equipment");
                    return false;
                }

                $("#signuptrans3").hide();
                $("#signuptrans4").show();
            }

            // Jaskaran - function to save selected Categories while signing up transporter
            $scope.SaveCategories = function () {

                $scope.categoriesArray = [];
                angular.forEach($scope.CategoryProduct, function (categories) {
                    if (categories.selected) $scope.categoriesArray.push(categories.name);

                });
                if ($scope.categoriesArray == "") {
                    toaster.pop('error', "You need to select atleast one category of the product");
                    return false;
                }

                console.log($scope.categoriesArray)
                $("#signuptrans4").hide();
                $("#signuptrans5").show();
            }
            // Jaskaran - function to save selected Cities for international while signing up transporter
            $scope.SaveInterCities = function () {
                $scope.CitiesArray = [];
                angular.forEach($scope.InternationCities, function (cities) {
                    if (cities.selected) $scope.CitiesArray.push(cities.name);

                });
                console.log($scope.CitiesArray)

                if ($scope.transporter.Domestic == true) {
                    $scope.transporter.International = false;
                    $scope.CitiesArray = [];
                } else {
                    $scope.transporter.Domestic = false;
                }


                console.log($scope.transporter.Domestic)
                console.log($scope.transporter.International)
                console.log($scope.CitiesArray)

                $("#signuptrans5").hide();
                $("#signuptrans6").show();
            }


            $scope.languageoptions = [];
            $scope.languageselected = [];

            $scope.settings = {
                scrollable: true,
                scrollableHeight: "250px",
                enableSearch: true,
                externalIdProp: ''
            }

            $scope.CheckInternational = function () {
                console.log($scope.transporter.International)
                if ($scope.transporter.International == true) {
                    $scope.Cities = true;
                } else {
                    $scope.Cities = false;
                }
            }

            //Jaskaran - function to get all the languages
            function GetLanguages() {
                var data = {
                    skip: 0,
                    limit: 0
                };

                console.log(data);
                //return false;
                Services.getlanguage(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {

                        for (var i = 0; i < result.data.length; i++) {
                            var obj = result.data[i];
                            var objl = {};
                            objl.id = obj.name;
                            objl.label = obj.name

                            $scope.languageoptions.push(objl);
                        }
                        //console.log($scope.languageoptions)
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }

                });
            }

            //Jaskaran - function to close the modal
            $scope.close = function () {
                $uibModalInstance.dismiss('close');
            };

            var LinkData = {
                'email': '',
                'linkedInId': ''
            };

            $scope.transporter = {
                CompanyName: '',
                Signemail: ''
            };
            $rootScope.$on('event:social-sign-in-success', function (event, userDetails) {
                console.log(userDetails);
                LinkData.email = userDetails.email;
                LinkData.linkedInId = userDetails.uid;

                console.log(LinkData);

                Services.Transporterlogin(LinkData, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        toaster.pop('success', "Login successful");
                        sessionStorage.setItem("Accesstoken", result.data.accessToken);
                        $cookies.put('token', result.data.accessToken);
                        $cookies.put('CustorTrans', "Transporter");
                        location.reload();
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.pop('info', "You need to Sign up");
                        $("#home").hide();
                        $("#signuptrans2").show();
                        $cookies.put('LinkedUEmail', LinkData.email);
                        $cookies.put('LinkedUName', userDetails.name);
                        $scope.transporter.Signemail = LinkData.email;
                        $scope.transporter.CompanyName = userDetails.name;

                    }
                });

            })


            //Jaskaran - function to login transporter
            $scope.LoginTransporter = function () {
                var data = {
                    email: $scope.transporter.email,
                    password: $scope.transporter.password
                };

                console.log(data);
                //return false;
                Services.Transporterlogin(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        toaster.pop('success', "Login successful");
                        sessionStorage.setItem("Accesstoken", result.data.accessToken);
                        $cookies.put('token', result.data.accessToken);
                        $cookies.put('CustorTrans', "Transporter");

                        location.reload();
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', "Invalid email id or password");
                        //  toaster.pop('error', error.message);
                    }
                });
            }
            //Jaskaran - function to go to next page while signing up transporter
            $scope.signupcheck1 = function () {
                $("#signuptrans2").hide();
                $("#signuptrans3").show();
            }

            //Jaskaran - function to go to last page while signing up transporter
            $scope.signupcheck2 = function () {
                $("#signuptrans6").hide();
                $("#signuptrans7").show();
            }
            $scope.TransCountry = "India";
            $scope.SignUpTransporter = function () {

                var TransportTraces = "";
                if ($scope.transporter.TraceYes == true) {
                    TransportTraces = "true"
                } else {
                    TransportTraces = "false"
                }



                var data = {

                    "Name": $scope.transporter.CompanyName,
                    "email": $scope.transporter.Signemail,
                    "contact": $scope.transporter.Primphone,
                    "secContact": $scope.transporter.Secphone,
                    "address": $scope.transporter.StreetAddress,
                    "city": $scope.transporter.City,
                    "pin": $scope.transporter.Pincode,
                    "country": $scope.TransCountry,
                    "about": $scope.transporter.About,
                    "majorClients": $scope.transporter.Majorclient,
                    "company": $scope.transporter.CompanyName,
                    "password": $scope.transporter.Signpassword,
                    "domestic": $scope.transporter.Domestic,
                    "viaLinkedIn": "false",
                    "tracing": TransportTraces,
                    "fleetsize": $scope.transporter.truckfleet,
                    "equipments": $scope.equipmentsArray,
                    "cities": $scope.CitiesArray,
                    "products": $scope.categoriesArray
                };

                console.log(JSON.stringify(data, undefined, 2));
                //return false;
                Services.transporterSignUp(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        if (result.statusCode == 200) {
                            toaster.pop('success', "Signup successful");
                            $cookies.put('token', result.data.accessToken);
                            $cookies.put('CustorTrans', "Transporter");
                            location.reload();
                        }
                        //toaster.pop('success', result.message);
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        if (error.responseType == "EMAIL_NUMBER_NO_UNIQUE") {
                            $("#signuptrans7").hide();
                            $("#signuptrans2").show();
                        }
                        toaster.pop('error', error.message);
                    }
                });
            }
            $scope.TransUpload = [];
            $scope.uploadImages = function (files) {
                $scope.TransUpload = files;
                console.log($scope.TransUpload)
            };
            // function MyAppController($scope) {
            //     $scope.rmConfig1 = {
            //         mondayStart: false,
            //         initState: "month", /* decade || year || month */
            //         maxState: "decade",
            //         minState: "month",
            //         decadeSize: 12,
            //         monthSize: 42, /* "auto" || fixed nr. (35 or 42) */
            //         min: new Date("2010-10-10"),
            //         max: null,
            //         format: "yy" /* https://docs.angularjs.org/api/ng/filter/date */
            //     }
            //     $scope.oDate1 = new Date();
            // }

        }])



    .controller('LogoutCtrl', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$uibModalInstance', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $uibModalInstance, $cookies) {

            $scope.init = function () {

            }
            $scope.logout = function () {
                $cookies.remove("token")
                $cookies.remove("LoggedInUser")
                $cookies.remove("CustorTrans")
                sessionStorage.removeItem("Accesstoken");

                console.log("logounttt")
                location.href = "#/nq/home"
                location.reload();

                //$state.go('header', {}, { reload: true });

            }

            //Jaskaran - function to close the modal
            $scope.close = function () {
                $uibModalInstance.dismiss('close');
            };

        }])


function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}