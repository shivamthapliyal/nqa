'use strict';
angular.module('Nqliast')
    .controller('MyBidsTranporterController', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $cookies) {



            $scope.quotation = "";

            $scope.init = function () {

                $scope.Accesstoken = $cookies.get('token');
                console.log("TOKEN-------" + $scope.Accesstoken);
                $scope.setTab(1);
            }
            $scope.tab = 1;

            $scope.setTab = function (newTab) {
                $scope.tab = newTab;
                if ($scope.tab == 1) {
                  //  $scope.GetOpenQuotation();
                }
                  if ($scope.tab == 3) {
                 //   $scope.GetCloseQuotation();
                }
            };

            $scope.isSet = function (tabNum) {
                return $scope.tab === tabNum;
            };

            $scope.GetOpenQuotation = function () {
                var data = {
                    "authorization": "aaa" //$scope.Accesstoken
                };
                console.log(data);
                //return false;

                Services.openBidQuotation(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        $scope.OpenQuotData = result.data;
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }

                });
            }
            $scope.GetCloseQuotation = function () {
                var data = {
                    "authorization": "aaa"//$scope.Accesstoken
                };

                console.log(data);
                //return false;
                Services.closeBidQuotation(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        $scope.OpenQuotData = result.data;
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }

                });
            }
        }])




function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}