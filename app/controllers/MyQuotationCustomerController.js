'use strict';
angular.module('Nqliast')
    .controller('MyQuotationCustomerController', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $cookies) {



            $scope.quotation = "";

            $scope.init = function () {

                $scope.Accesstoken = $cookies.get('token');
                console.log("TOKEN-------" + $scope.Accesstoken);
                $scope.setTab(1);
            }
            $scope.tab = 1;

            $scope.setTab = function (newTab) {
                $scope.tab = newTab;
                if ($scope.tab == 1) {
                    $scope.GetOpenQuotation();
                    toaster.pop('wait',"Loading..")
                }
                if ($scope.tab == 3) {
                    // $scope.GetCloseQuotation();
                }
            };

            $scope.isSet = function (tabNum) {
                return $scope.tab === tabNum;
            };

            $scope.GetOpenQuotation = function () {
                var header = {
                    "authorization": 'beares ' + $cookies.get('token')
                }
                var data = "";
                console.log(header);
                //return false;

                Services.openQuotationLisitng(header, data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        $scope.OpenQuotData = result.data;
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                      // toaster.pop('error', error.message);
                    }

                });
            }


            $scope.ViewOpenQuotation = function (data) {
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'ViewOpenQuot.html',
                    controller: 'ViewOpenQuotCtrl',
                    size: '',
                    resolve: {
                        items: function () {
                            return {
                                data: data
                            };
                        }
                    }
                });

            };



            $scope.GetCloseQuotation = function () {
                var data = {
                    "authorization": "aaa"//$scope.Accesstoken
                };

                console.log(data);
                //return false;
                Services.closeBidQuotation(data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
                    if (result.statusCode == 200) {
                        $scope.OpenQuotData = result.data;
                    }
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }

                });
            }
        }])

    .controller('ViewOpenQuotCtrl', function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $uibModalInstance, $cookies, items) {


        $scope.QuationId = items.data._id;
        $scope.LeaveStreet = items.data.destinationAddress[0].street;
        $scope.LeaveCity = items.data.destinationAddress[0].city;
        $scope.LeaveCountry = items.data.destinationAddress[0].country;
        $scope.LeavePin = items.data.destinationAddress[0].pin;
        $scope.GoingStreet = items.data.leaveAddress[0].street;
        $scope.GoingCity = items.data.leaveAddress[0].city;
        $scope.GoingCountry = items.data.leaveAddress[0].country;
        $scope.GoingPin = items.data.leaveAddress[0].pin;
        $scope.DepartDate = new Date(items.data.departureDate);
        $scope.Load = items.data.loadDetails;
        $scope.Trip = items.data.tripContract;
        $scope.Description = items.data.description;
        $scope.QuotationCarrier = items.data.quotationCarrier;
        $scope.Price = items.data.price;

        console.log($scope.DepartDate)

        //Jaskaran - function to close the modal
        $scope.close = function () {
            $uibModalInstance.dismiss('close');
        };

    })


function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}