'use strict';
angular.module('Nqliast')
    .controller('MessagesCustomerController', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $cookies) {


            $scope.init = function () {

                $scope.setTab(1);

                $scope.messagedata = [
                    {
                        "id": 1,
                        "name": "ABC",
                        "Price": "$10"
                    },
                    {
                        "id": 2,
                        "name": "DEF",
                        "Price": "$20"
                    },
                    {
                        "id": 3,
                        "name": "GHI",
                        "Price": "$30"
                    },
                    {
                        "id": 4,
                        "name": "JKL",
                        "Price": "$40"
                    }
                ]

            }
            $scope.tab = 1;

            $scope.setTab = function (newTab) {
                $scope.tab = newTab;
                console.log($scope.tab)
            };

            $scope.isSet = function (tabNum) {
                return $scope.tab === tabNum;
            };

        }])




function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}