'use strict';
angular.module('Nqliast')
    .controller('RequestQuotationCustomerController', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', '$uibModal', '$cookies', 'Services',
        function ($scope, $location, $state, toaster, $stateParams, $filter, $uibModal, $cookies, Services) {

            $scope.quotation = "";
            $scope.QuotationCountryLeave = "India";
            $scope.QuotationCountryGoing = "India";

            //$scope.Accesstoken = sessionStorage.getItem("Accesstoken");
            console.log($cookies.get('token'));
     
            $scope.LoggedUserData = JSON.parse(sessionStorage.getItem("LoggedInUser"));
            console.log($scope.LoggedUserData);


            $scope.init = function () {

            }
            ///-----------------Equipments array
            $scope.TransportEquipments = [{
                id: 1,
                name: 'Car Transporter',
                pic: 'img/assets/car_transporter_home_page.png',
                class: 'pull-right'
            },
            {
                id: 2,
                name: 'Bus',
                pic: 'img/assets/bus_homepage.png',
                class: 'pull-left'
            },
            {
                id: 3,
                name: 'Tipper',
                pic: 'img/assets/tipper_homepage.png',
                class: 'pull-right'
            },
            {
                id: 4,
                name: 'Reefer',
                pic: 'img/assets/reefer_homepage.png',
                class: 'pull-left'
            },
            {
                id: 5,
                name: 'Tanker',
                pic: 'img/assets/tanker_homepage.png',
                class: 'pull-right'
            },
            {
                id: 6,
                name: 'Lowbed',
                pic: 'img/assets/lowbed_homepage.png',
                class: 'pull-left'
            },
            {
                id: 7,
                name: 'Flatbed',
                pic: 'img/assets/flatbed_homepage.png',
                class: 'pull-right'
            },
            {
                id: 8,
                name: 'Curtainside',
                pic: 'img/assets/curtainside_homepage.png',
                class: 'pull-left'
            },
            {
                id: 9,
                name: 'Container',
                pic: 'img/assets/container_homepage.png',
                class: 'pull-right'
            },
            {
                id: 10,
                name: 'Pickup',
                pic: 'img/assets/pickup_homepage.png',
                class: 'pull-left'
            }];
            // ------------------------Load array -----------------------
            $scope.LoadFill = [{
                id: 1,
                name: 'Palletized Cargo',
                pic: 'img/assets/pallitized_goodsf.png'
            },
            {
                id: 2,
                name: 'Food Items',
                pic: 'img/assets/food_items.png'
            },
            {
                id: 3,
                name: 'Dangerous Goods',
                pic: 'img/assets/dangerous_goods.png'
            },
            {
                id: 4,
                name: 'Construction Materials',
                pic: 'img/assets/construction_materials.png'
            },
            {
                id: 5,
                name: 'Machinery',
                pic: 'img/assets/machinery.png'
            },
            {
                id: 6,
                name: 'Others',
                pic: 'img/assets/others.png'
            },
            {
                id: 7,
                name: 'Steel Bars & Pipes',
                pic: 'img/assets/steel_bars.png'
            },
            {
                id: 8,
                name: 'Bulk materials',
                pic: 'img/assets/bulk_materials.png'
            }];

            //function MyAppController($scope) {
            $scope.rmConfig1 = {
                mondayStart: false,
                initState: "month", /* decade || year || month */
                maxState: "decade",
                minState: "month",
                decadeSize: 12,
                monthSize: 42, /* "auto" || fixed nr. (35 or 42) */
                min: new Date("2010-10-10"),
                max: null,
                format: "MM/dd/yyyy" /* https://docs.angularjs.org/api/ng/filter/date */
            }
            $scope.DeparDate = new Date();
            //}
            $scope.AddQuotation = function () {


                $scope.equipmentsArray = [];
                angular.forEach($scope.TransportEquipments, function (Equipments) {
                    if (Equipments.selected) $scope.equipmentsArray.push(Equipments.name);
                });
                if ($scope.equipmentsArray == "") {
                    toaster.pop('error', "You need to select atleast any one of the vehicle");
                    return false;
                }


                $scope.LoadDetailsArray = [];
                angular.forEach($scope.LoadFill, function (Equipments) {
                    if (Equipments.selected) $scope.LoadDetailsArray.push(Equipments.name);
                });
                if ($scope.LoadDetailsArray == "") {
                    toaster.pop('error', "You need to select atleast one of the Load");
                    return false;
                }


                var singleTrace = "";
                if ($scope.TraceSingle == true) {
                    singleTrace = true;
                } else {
                    singleTrace = false;
                }
                var openbid = "";
                if ($scope.quotation.acceptcheck == true) {
                    openbid = true;
                } else {
                    openbid = false;
                }

                var headers = {
                    "authorization": 'bearer ' +  $cookies.get('token')
                }

                var data = {
                    "quotationby": $scope.LoggedUserData._id,
                    "openbid": true,
                    "trips": parseInt($scope.quotation.trips),
                    "truckWeight": parseInt($scope.quotation.truckfleet),
                    "quotationCarrier": $scope.equipmentsArray,
                    "price": parseInt($scope.quotation.price),
                    "contractDuration": $scope.quotation.duration,
                    "loadDetails": $scope.LoadDetailsArray,
                    "truckFloor": parseInt($scope.quotation.floor),
                    "departureDate": $scope.DeparDate,
                    "description": $scope.quotation.description,
                    "destinationAddress": [
                        {
                            "street": $scope.quotation.streetgoing,
                            "city": $scope.quotation.citygoing,
                            "country": $scope.QuotationCountryGoing,
                            "pin": $scope.quotation.pincodegoing,
                        }
                    ],
                    "leaveAddress": [{
                        "street": $scope.quotation.streetleave,
                        "city": $scope.quotation.cityleave,
                        "country": $scope.QuotationCountryLeave,
                        "pin": $scope.quotation.pincodeleave,
                    }],
                    "singleTrip": singleTrace
                }
                console.log(JSON.stringify(headers, undefined, 2));
                console.log(JSON.stringify(data, undefined, 2));
                //   return false;
                Services.addQuotation(headers, data, function (result) {
                    console.log(JSON.stringify(result, undefined, 2));
         
                        if (result.statusCode == 200) {
                            toaster.pop('success', "You have requested an Quotation");
                            $state.go('app.my-quotations');
                            //location.reload();
                        }
                        //toaster.pop('success', result.message);
                    
                }, function (error) {
                    if (error == null) {
                        toaster.pop('error', "Could'nt connect to server! Please try again later");
                    } else {
                        console.log(error);
                        toaster.clear();
                        toaster.pop('error', error.message);
                    }
                });


            }
            $scope.uploadImages = function (files) {
                $scope.TransUpload = files;
                console.log($scope.TransUpload)
            };

            // ///// funtion for formating date 
            // $scope.convertDate = function (datestring) {

            //     var stDate = new Date(rectifyFormat(datestring));
            //     stDate.toString();
            //     return $filter('date')(new Date(stDate), 'MMM/dd/yyyy');               
            // }

            // function rectifyFormat(datestring) {
            //     var dt = datestring.substring(0, datestring.length - 2);
            //     var ampm = datestring.substring(datestring.length - 2);
            //     return dt + " " + ampm + " UTC";
            // }
        }])





function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}