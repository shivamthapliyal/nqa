'use strict';
angular.module('Nqliast')
    .controller('ProfileTransporterController', ['$scope', '$location', '$state', 'toaster', '$stateParams', '$filter', 'Services', '$uibModal', '$cookies',
        function ($scope, $location, $state, toaster, $stateParams, $filter, Services, $uibModal, $cookies) {

            $scope.customer = "";

            $scope.init = function () {

                if (typeof ('storage') != undefined) {    
                                
                    if (sessionStorage.getItem("LoggedInUser") != undefined) {
                        $scope.LoggedUserData = sessionStorage.getItem("LoggedInUser");

                        console.log($scope.LoggedUserData);
                    }
                }
                $scope.Accesstoken = $cookies.get('token');
                console.log("TOKEN-------" + $scope.Accesstoken);
                $scope.setTab(1);
            }
            $scope.tab = 1;

            $scope.setTab = function (newTab) {
                $scope.tab = newTab;
                if ($scope.tab == 1) {
                }
                  if ($scope.tab == 3) {
                }
            };

            $scope.isSet = function (tabNum) {
                return $scope.tab === tabNum;
            };

        }])




function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}